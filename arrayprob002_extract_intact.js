//You want to extract out a portion of an array but keep the original array
//intact.

var animal=["dog","cat","cat","seal","elephant","walrus","lion","dog","dog"];
var animal02 =animal.splice(1, 3);
console.log(animal);
console.log(animal02);
console.log(animal);